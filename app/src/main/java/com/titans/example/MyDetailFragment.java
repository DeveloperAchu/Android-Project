package com.titans.example;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MyDetailFragment extends Fragment {
    private TextView textView;
    private String selectedName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detail_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            selectedName = ((MainActivity)getActivity()).getSelectedName();

            textView = (TextView) getActivity().findViewById(R.id.nameDetailTextView);
            textView.setText(selectedName);
            ((MainActivity)getActivity()).setListFragment(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
