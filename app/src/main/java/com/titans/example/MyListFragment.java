package com.titans.example;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

public class MyListFragment extends Fragment {
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MainActivity)getActivity()).setListFragment(true);
        listView = (ListView) getActivity().findViewById(R.id.nameList);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(), R.array.names, android.R.layout.simple_list_item_1);
        listView.setAdapter(adapter);
        if(((MainActivity) getActivity()).isInitial()) {
            ((MainActivity) getActivity()).setSelectedName((String) adapter.getItem(0));
            ((MainActivity)getActivity()).setInitial(false);
        }

        listView.setOnItemClickListener(
                new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        String name = (String) parent.getItemAtPosition(position);
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

                        FrameLayout frameLayout = (FrameLayout) getActivity().findViewById(R.id.tabletListContainer);
                        if (frameLayout != null) {
                            fragmentTransaction.replace(R.id.tabletDetailContainer, new MyDetailFragment());
                        } else {
                            fragmentTransaction.replace(R.id.phoneLayoutContainer, new MyDetailFragment());
                        }
                        ((MainActivity)getActivity()).setSelectedName(name);
                        fragmentTransaction.commit();

                    }
                }
        );

    }

}
