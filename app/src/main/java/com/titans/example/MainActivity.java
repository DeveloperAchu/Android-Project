package com.titans.example;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {
    private boolean tabletUIEnabled = false;
    private boolean listFragment = true;
    private String selectedName = "";
    private boolean initial = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        tabletUIEnabled = checkIfTabletUILoaded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (tabletUIEnabled) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.tabletListContainer, new MyListFragment());
            fragmentTransaction.replace(R.id.tabletDetailContainer, new MyDetailFragment());
            fragmentTransaction.commit();
        } else {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            if (listFragment) {
                fragmentTransaction.replace(R.id.phoneLayoutContainer, new MyListFragment());
            } else {
                fragmentTransaction.replace(R.id.phoneLayoutContainer, new MyDetailFragment());
            }
            fragmentTransaction.commit();
        }
    }

    private boolean checkIfTabletUILoaded() {
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.tab);
        if (relativeLayout == null)
            return false;

        return true;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("listFragment", listFragment);
        outState.putString("selectedName", selectedName);
        outState.putBoolean("initial", initial);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        listFragment = savedInstanceState.getBoolean("listFragment");
        selectedName = savedInstanceState.getString("selectedName");
        initial = savedInstanceState.getBoolean("initial");
    }

    @Override
    public void onBackPressed() {
        if (!listFragment && !tabletUIEnabled) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.phoneLayoutContainer, new MyListFragment());
            fragmentTransaction.commit();
            return;
        }
        super.onBackPressed();
    }

    public void setListFragment(boolean listFragment) {
        this.listFragment = listFragment;
    }

    public void setSelectedName(String selectedName) {
        this.selectedName = selectedName;
    }

    public String getSelectedName() {
        return selectedName;
    }

    public boolean isInitial() {
        return initial;
    }

    public void setInitial(boolean initial) {
        this.initial = initial;
    }
}
